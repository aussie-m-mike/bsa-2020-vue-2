import Vue from 'vue'
import VueRouter from 'vue-router'

import Navigation from '@/components/Navigation.vue';
import User from '@/views/User.vue';
import AddUser from '@/views/AddUser.vue';
import ViewUser from '@/views/ViewUser.vue';
import Album from '@/views/Album.vue';
import AlbumForm from '@/components/AlbumForm.vue';

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        redirect: 'albums'
    },
    {
        path: '/users',
        name: 'users',
        components: { default: User, navigation: Navigation }
    },
    {
        path: '/users/add',
        name: 'users.add',
        components: { default: AddUser, navigation: Navigation}
    },
    {
        path: '/users/:id',
        name: 'users.view',
        components: { default: ViewUser, navigation: Navigation}
    },
    {
        path: '/users/:id/edit',
        name: 'users.edit',
        components: { default: AddUser, navigation: Navigation}
    },
    {
        path: '/albums',
        name: 'albums',
        components: { default: Album, navigation: Navigation }
    },
    {
        path: '/albums/add/:id',
        name: 'albums.add',
        components: { default: AlbumForm, navigation: Navigation }
    },
    {
        path: '/albums/:id',
        name: 'albums.view'
    },
    {
        path: '/albums/:id/edit',
        name: 'albums.edit'
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
