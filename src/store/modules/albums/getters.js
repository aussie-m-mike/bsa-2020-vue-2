export default {
    totalAlbums: state => state.albums.length,
    findAlbumById: state => (id) => {
        return state.albums.map((album) => {
            return album.id
        }).indexOf(id);
    }
}
