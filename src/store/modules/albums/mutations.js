export default {
    ADD_ALBUM(state, album) {
        state.albums.push({...album, id: Math.floor(Math.random() * 100)});
    },
    DELETE_ALBUM(state, index) {
        state.albums.splice(index, 1);
    },
    SET_ALBUMS(state, albums) {
        state.albums = albums;
    }
}