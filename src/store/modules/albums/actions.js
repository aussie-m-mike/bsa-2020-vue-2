import axios from 'axios';

const API_URL = 'https://my-json-server.typicode.com/aussie-m-mike/bsa-2020-vue2/albums';

export default {
    async fetchAlbums({ commit }) {
        try {
            const response = await axios.get(API_URL);
            commit('SET_ALBUMS', response.data);
        } catch (error) {
            return Promise.reject(error);
        }
    },
    saveAlbum({ commit }, albumData) {
        if(albumData.id) {
            commit('UPDATE_ALBUM', { albumData }, albumData.id);
        } else {
            commit('ADD_ALBUM', albumData);
        }
    },
    deleteAlbum({commit, getters}, album) {
        const index = getters.findAlbumById(album.id);
        commit('DELETE_ALBUM', index);
    }
}