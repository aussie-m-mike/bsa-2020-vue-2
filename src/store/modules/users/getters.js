export default {
    totalUsers: state => state.users.length,
    findUserById: state => (id) => {
        return state.users.map((user) => {
            return user.id
        }).indexOf(id);
    },
}