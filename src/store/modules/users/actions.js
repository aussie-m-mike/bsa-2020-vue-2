import axios from 'axios';

const API_URL = 'https://my-json-server.typicode.com/aussie-m-mike/bsa-2020-vue2/users';

export default {
    async fetchUsers({ commit }) {
        try {
            const response = await axios.get(API_URL);
            commit('SET_USERS', response.data);
        } catch (error) {
            return Promise.reject(error);
        }
    },
    saveUser({ commit }, userData) {
        if(userData.id) {
            commit('UPDATE_USER', { userData }, userData.id);
        } else {
            commit('ADD_USER', userData);
        }
    },
    deleteUser({commit, getters}, user) {
        const index = getters.findUserById(user.id);
        commit('DELETE_USER', index);
    }
}