export default {
    ADD_USER(state, user) {
        state.users.push({...user, id: Math.floor(Math.random() * 100)});
    },
    UPDATE_USER(state, user, id) {
      state.users[id] = user;
    },
    DELETE_USER(state, index) {
        state.users.splice(index, 1);
    },
    SET_USERS(state, users) {
        state.users = users;
    }
}